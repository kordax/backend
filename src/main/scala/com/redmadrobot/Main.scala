package com.redmadrobot

import java.sql.SQLTimeoutException

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.redmadrobot.chatlib.adapters.db.PostgreSQLAdapter
import com.redmadrobot.chatlib.adapters.kafka.{KafkaAdapter, PBMessageDeserializer, PBMessageSerializer}
import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import com.redmadrobot.config.{ArgsParser, Config}
import com.redmadrobot.logic.KafkaConsumerManager
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.log4s.getLogger

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Main application object
  */
object Main extends App {
  private val LOGGER = getLogger

  LOGGER.info("Starting backend server")

  val config = parseArgs()

  implicit val system: ActorSystem = ActorSystem("akka-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  if (config.host.isEmpty && config.kafka.isEmpty) {
    LOGGER.error(s"Nor RabbitMQ neither Kafka host were provided")
    sys.exit(0)
  }

  val pAdapter: PostgreSQLAdapter = new PostgreSQLAdapter(config.dbConnStr)

  try {
    pAdapter.connect(30000)
  } catch {
    case _: SQLTimeoutException =>
      LOGGER.error(s"Connection timeout")
      sys.exit(0)
    case default: Throwable =>
      LOGGER.error(s"Connection failed: ${default.getMessage}")
      sys.exit(0)
  }

  if (!config.kafka.isEmpty) {
    implicit val kafkaConsumer: KafkaAdapter[String, PBMessage] = new KafkaAdapter[String, PBMessage](
      config.kafka,
      "backend",
      keySerializer = new StringSerializer,
      valueSerializer = new PBMessageSerializer,
      keyDeserializer = new StringDeserializer,
      valueDeserializer = new PBMessageDeserializer
    )

    val consumer: KafkaConsumerManager = new KafkaConsumerManager(config.dbConnStr, config.poolSize)

    try {
      consumer.subscribe(30000)
    } catch {
      case default: Exception =>
        LOGGER.error(default.getMessage)
        sys.exit(0)
    }
  }

  sys.addShutdownHook({
    system.terminate()
    LOGGER.info("Shutting down backend server")
  })

  /**
    * Parse application arguments
    */
  private def parseArgs(): Config = {
    ArgsParser.parse(args)
    val config = ArgsParser.getConfig
    ArgsParser.validateArgs()

    config
  }
}