package com.redmadrobot.logic

import java.io.IOException
import java.util.regex.Pattern

import akka.actor.{ActorSystem, Props}
import com.redmadrobot.chatlib.adapters.kafka.KafkaAdapter
import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import com.redmadrobot.logic.actors.KafkaActorDispatcher
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.log4s.getLogger

import scala.concurrent.TimeoutException

/**
  * Kafka consumer manager
  *
  * @param dbConnStr database connection string
  * @param poolSize workers pool size
  * @param adapter implicit database adapter
  */
class KafkaConsumerManager(dbConnStr: String, poolSize: Int)(implicit adapter: KafkaAdapter[String, PBMessage]) {
  private val LOGGER = getLogger

  private implicit val system: akka.actor.ActorSystem = ActorSystem("ActorManagerSystem")
  private val actorManager = system.actorOf(Props(new KafkaActorDispatcher(dbConnStr, poolSize)), name = "KafkaActorManager")

  private val consumer: KafkaConsumer[String, PBMessage] = adapter.getConsumer
  private var consumerThread: Thread = _
  private var subscribed: Boolean = false

  /**
    * Subscribe consumer
    *
    * @param timeoutMS timeout in milliseconds
    * @throws TimeoutException if kafka subscription timeout has occurred
    * @throws IOException if kafka unknown error has occurred
    */
  @throws(classOf[TimeoutException])
  @throws(classOf[IOException])
  def subscribe(timeoutMS: Int): Unit = {
    LOGGER.debug("Subscribing KafkaConsumer")

    if (subscribed) {
      LOGGER.error("Already subscribed")
      return
    }

    consumer.subscribe(Pattern.compile(KafkaAdapter.KAFKA_TASKS_TOPIC))
    subscribed = true

    consumerThread = new Thread() {
      override def run(): Unit = {
        while (subscribed) {
          try {
            val it = consumer.poll(60000).iterator()

            while (it.hasNext && subscribed) {
              actorManager ! it.next().value()
            }
          } catch {
            case e: Exception => LOGGER.error(s"${e.getMessage}")
          }
        }
      }
    }

    consumerThread.start()

    LOGGER.info(s"KafkaConsumer successfully subscribed to topics")
  }

  /**
    * Unsubscribes current consumer from MQ
    *
    * @throws java.io.IOException if any error occurred
    */
  @throws(classOf[IOException])
  def unsubscribe(): Unit = {
    LOGGER.debug(s"Unsubscribing KafkaConsumer ")

    if (!subscribed) {
      LOGGER.error("Not subscribed to any topics")
      return
    }

    consumer.unsubscribe()
    subscribed = false
    consumerThread.join()
  }
}
