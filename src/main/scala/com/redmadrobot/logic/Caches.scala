package com.redmadrobot.logic

import scala.collection.parallel.mutable.ParArray

/**
  * Main container class for all caches
  *
  */
object Caches {
  val dbRoomIDCache = new DBCache[Long]
  val dbRoomClientsCache = new DBCache[ParArray[String]]
  val dbSessionsCache = new DBCache[String]
}
