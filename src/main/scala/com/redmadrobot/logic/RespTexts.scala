package com.redmadrobot.logic

object RespTexts {
  val TEXT_RESP_STATUS_SUCC = "Everything is ok"
  val TEXT_RESP_STATUS_FAIL = "Status operation failed"
  val TEXT_RESP_JOIN_ROOM_SUCC = "Successfully joined room"
  val TEXT_RESP_JOIN_ROOM_FAIL = "Failed to join room"
  val TEXT_RESP_LEAVE_ROOM_SUCC = "Successfully leaved room"
  val TEXT_RESP_LEAVE_ROOM_FAIL = "Failed to leave room"
  val TEXT_RESP_CREATE_ROOM_SUCC = "Successfully created room"
  val TEXT_RESP_CREATE_ROOM_FAIL = "Failed to create room"
  val TEXT_RESP_POST_MSG_SUCC = "Post command succeeded"
  val TEXT_RESP_POST_MSG_FAIL = "Post command failed"
}
