package com.redmadrobot.logic.actors

import java.sql.{SQLException, SQLTimeoutException}

import akka.actor.Actor
import com.redmadrobot.chatlib.adapters.db.PostgreSQLAdapter
import com.redmadrobot.chatlib.adapters.kafka.KafkaAdapter
import com.redmadrobot.chatlib.common.TaskState
import com.redmadrobot.chatlib.logging.LoggingHelpers
import com.redmadrobot.chatlib.protobuf.messages.ResponseErrorCode.{RESP_CREATE_ROOM_FAIL, RESP_ERR_UNKNOWN, RESP_JOIN_ROOM_FAIL, RESP_LEAVE_ROOM_FAIL, RESP_POST_FAIL}
import com.redmadrobot.chatlib.protobuf.messages.ResponseMessage.{ResponseErrorMessage, ResponseStatusMessage}
import com.redmadrobot.chatlib.protobuf.messages.ResponseStatusCode.{RESP_CREATE_ROOM_SUCC, RESP_JOIN_ROOM_SUCC, RESP_POST_SUCC, RESP_STATUS_SUCC}
import com.redmadrobot.chatlib.protobuf.messages._
import com.redmadrobot.logic.RespTexts._
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.log4s.getLogger

import scala.collection.parallel.mutable.ParArray

/**
  * Main worker actor that's responsible for low-level logic
  * For performance reasons this class shouldn't implement any logging
  *
  */
class KafkaWorker(dbConnStr: String)(implicit producer: KafkaProducer[String, PBMessage]) extends Actor {
  private val LOGGER = getLogger
  val dbAdapter: PostgreSQLAdapter = new PostgreSQLAdapter(dbConnStr)

  LOGGER.info(s"Starting worker: ${self.toString().substring(self.toString().indexOf("KafkaWorkerActors/") + "KafkaWorkerActors/".length, self.toString().length)}")

  val dbThread: Thread = new Thread() {
    override def run(): Unit = {
      while (true) {
        if (!dbAdapter.isConnected) {
          try {
            dbAdapter.connect(10000)
          } catch {
            case _: SQLTimeoutException =>
              LOGGER.error(s"Connection timeout")
            case default: Throwable =>
              LOGGER.error(s"Connection failed: ${default.getMessage}")
          }
        }

        Thread.sleep(10000)
      }
    }
  }
  dbThread.start()

  /**
    * Handle received message
    *
    */
  override def receive: PartialFunction[Any, Unit] = {
    case message: PBMessage =>
      LOGGER.debug("Received message")
      val clientName = message.clientName
      try {
        try {
          LOGGER.debug("Getting message type")
          val receivedType = message.getTask.`type`

          // Store
          LOGGER.debug("Storing task audit")
          dbAdapter.insertTaskAudit(message, TaskState.RECEIVED)

          var response: PBMessage = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName)

          if (receivedType == TaskMessageType.TASK_STATUS) {
            LOGGER.debug(s"""Received TASK_STATUS message""")
            response = response.withResponse(
              ResponseMessage(ResponseMessageType.RESP_STATUS).withStatusMsg(ResponseStatusMessage(RESP_STATUS_SUCC,
                Some(TEXT_RESP_STATUS_SUCC))
              )
            )
            LOGGER.debug(TEXT_RESP_STATUS_SUCC)

            producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
          } else if (receivedType == TaskMessageType.TASK_POST) {
            LOGGER.debug(s"""Received TASK_POST message""")
            val postMsg = message.getTask.getPostMsg

            try {
              // Store message into database for a current chat group and send response to all chat members
              dbAdapter.insertChatMessage(postMsg.roomID, clientName, postMsg)

              var clients = new ParArray[String](0)
              clients = dbAdapter.getRoomClients(postMsg.roomID).par

              LOGGER.debug(s"""Message "${postMsg.data}" to room "${postMsg.roomID}" has been successfully posted""")
              LOGGER.debug(s"Number of room clients: ${clients.length}")

              for (client <- clients) {
                LOGGER.debug(s"Room client: $client")
              }

              clients.foreach({ client =>
                var response: PBMessage = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), client)
                response = response.withResponse(
                  ResponseMessage(ResponseMessageType.RESP_STATUS).withStatusMsg(ResponseStatusMessage(RESP_POST_SUCC,
                    Some(message.getTask.getPostMsg.data)))
                )

                LOGGER.debug(s"Sending to user topic ${KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + client}")
                producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + client, response))
              })

              producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            } catch {
              case e: Throwable =>
                response = response.withResponse(
                  ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_POST_FAIL,
                    Some(s"$TEXT_RESP_POST_MSG_FAIL: ${e.getMessage}")))
                )
                LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(e)}")

                producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            }
          } else if (receivedType == TaskMessageType.TASK_CREATE_ROOM) {
            LOGGER.debug(s"""Received TASK_CREATE_ROOM message""")
            val newChatMsg = message.getTask.getCreateRoomMsg

            var lastRoomID = dbAdapter.getLastRoomID
            val roomID = lastRoomID + 1L

            try {
              // Store message into database for a current chat group and send response to all chat members
              dbAdapter.insertCreateRoomMessage(clientName, roomID, newChatMsg)

              response = response.withResponse(
                ResponseMessage(ResponseMessageType.RESP_STATUS).withStatusMsg(ResponseStatusMessage(RESP_CREATE_ROOM_SUCC,
                  Some(s"""New room "${newChatMsg.roomName}" has been successfully created, room_id: $roomID""")).withData(roomID))
              )
              LOGGER.debug(s"""New room "${newChatMsg.roomName}" has been successfully created, room_id: $roomID""")

              producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            } catch {
              case e: Throwable =>
                response = response.withResponse(
                  ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_CREATE_ROOM_FAIL,
                    Some(s"$TEXT_RESP_CREATE_ROOM_FAIL: ${e.getMessage}")))
                )
                LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(e)}")

                producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            }
          } else if (receivedType == TaskMessageType.TASK_JOIN_ROOM) {
            LOGGER.debug(s"""Received TASK_JOIN_ROOM message""")
            val joinMsg = message.getTask.getJoinRoomMsg

            try {
              dbAdapter.insertClientToRoom(joinMsg.roomID, clientName)

              response = response.withResponse(
                ResponseMessage(ResponseMessageType.RESP_STATUS).withStatusMsg(ResponseStatusMessage(RESP_JOIN_ROOM_SUCC,
                  Some(TEXT_RESP_JOIN_ROOM_SUCC))
                )
              )
              LOGGER.debug(TEXT_RESP_JOIN_ROOM_SUCC)

              producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            } catch {
              case e: Throwable =>
                response = response.withResponse(
                  ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_JOIN_ROOM_FAIL,
                    Some(s"$TEXT_RESP_JOIN_ROOM_FAIL: ${e.getMessage}")))
                )
                LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(e)}")

                producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            }
          } else if (receivedType == TaskMessageType.TASK_LEAVE_ROOM) {
            LOGGER.debug(s"""Received TASK_LEAVE_ROOM message""")
            val leaveMsg = message.getTask.getLeaveRoomMsg

            try {
              dbAdapter.removeClientFromRoom(leaveMsg.roomID, clientName)

              response = response.withResponse(
                ResponseMessage(ResponseMessageType.RESP_STATUS).withStatusMsg(ResponseStatusMessage(RESP_JOIN_ROOM_SUCC,
                  Some(TEXT_RESP_LEAVE_ROOM_SUCC))
                )
              )
              LOGGER.debug(TEXT_RESP_LEAVE_ROOM_SUCC)

              producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            } catch {
              case e: Throwable =>
                response = response.withResponse(
                  ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_LEAVE_ROOM_FAIL,
                    Some(s"$TEXT_RESP_LEAVE_ROOM_FAIL: ${e.getMessage}")))
                )
                LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(e)}")

                producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, response))
            }
          } else {
            LOGGER.error(s"""Unknown message received!""")
          }
        } catch {
          case e: Throwable => LOGGER.error(e.getMessage)
        }
      } catch {
        case e: SQLException => LOGGER.error(e.getMessage)
        case e: Throwable =>
          LOGGER.error(s"Unknown exception: ${LoggingHelpers.getStackTraceAsString(e)}")
          val errResponse = PBMessage(MessageType.RESPONSE, System.currentTimeMillis(), clientName).withResponse(
            ResponseMessage(ResponseMessageType.RESP_ERROR).withErrorMsg(ResponseErrorMessage(RESP_ERR_UNKNOWN,
              Some(e.getMessage)))
          )

          producer.send(new ProducerRecord[String, PBMessage](KafkaAdapter.KAFKA_USER_TOPIC_PREFIX + clientName, errResponse))
      }
    case _ => LOGGER.debug("WorkerActor default received")
  }
}
