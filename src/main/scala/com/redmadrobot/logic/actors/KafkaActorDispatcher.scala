package com.redmadrobot.logic.actors

import akka.actor.{Actor, ActorRef, Props}
import com.redmadrobot.chatlib.adapters.kafka.KafkaAdapter
import com.redmadrobot.chatlib.protobuf.messages.PBMessage
import org.apache.kafka.clients.producer.KafkaProducer
import org.log4s.getLogger

/**
  * Parent actor class that is responsible child worker actors
  */
class KafkaActorDispatcher(dbConnStr: String, poolSize: Int = 0)(implicit kafAdapter: KafkaAdapter[String, PBMessage]) extends Actor {
  private val LOGGER = getLogger

  /** Actors pool size */
  var ACTORS_POOL_SIZE: Int = _
  if (poolSize <= 0) {
    ACTORS_POOL_SIZE = Runtime.getRuntime.availableProcessors * 2
  } else {
    ACTORS_POOL_SIZE = poolSize
  }
  LOGGER.info(s"Actor dispatcher number of workers: $ACTORS_POOL_SIZE")

  implicit val producer: KafkaProducer[String, PBMessage] = kafAdapter.getProducer

  /** Pool of worker actors */
  val workerActors: ActorRef = context.actorOf(akka.routing.RoundRobinPool(nrOfInstances = ACTORS_POOL_SIZE).props(Props(new KafkaWorker(dbConnStr))), name = "KafkaWorkerActors")

  /**
    * Main logic
    *
    * @return [[akka.actor.Actor.Receive]] expression
    */
  override def receive: Receive = {
    case request => workerActors ! request
  }
}
