package com.redmadrobot.logic

import java.sql.ResultSet

import com.redmadrobot.chatlib.protobuf.messages._
import org.log4s.getLogger

import scala.collection.mutable

/**
  * Main cache class that's capable of caching and retrieving cached DB results.
  * Cache map key consists of client name and last request timestamp in milliseconds,
  * value is a tuple of timestamp (in MS) and generic [[R]] as a result type
  *
  */
class DBCache[R] extends mutable.HashMap[(String, TaskMessageType), (Long, R)] {
  private val LOGGER = getLogger

  private val cacheLifeTimeMS = 60000
  private val cacheManagerHeartBeatMS = 5000

  private val cacheManagerT = new Thread(new DBCacheManager())
  // TODO: Thread state exceptions not handled
  cacheManagerT.start()

  /**
    * Put db result set to cache and remember it's timestamp
    * This method will replace old cached response for provided client name
    *
    * @param taskType task type (STATUS, AUTH... etc.)
    * @param resultSet result set
    * @throws IllegalArgumentException if message argument is invalid
    * @throws RuntimeException if message argument is invalid
    * @return cached response message instance reference
    */
  @throws(classOf[IllegalArgumentException])
  @throws(classOf[RuntimeException])
  def put(clientName: String, taskType: TaskMessageType, resultSet: R): R = {
    try {
      this.put((clientName, taskType), (System.currentTimeMillis(), resultSet))
      LOGGER.debug(s"Cached DB result for client, task type: $clientName, $taskType")
    } catch {
      case e: Throwable =>
        throw new RuntimeException(s"Failed to push result to cache: ${e.getMessage}")
    }

    resultSet
  }

  /**
    * Check if there's cached response present in this cache for this task message and return result of [[Option]] type
    *
    * @param clientName client name
    * @param taskType task message type [[TaskMessage]]
    * @return [[Option]] of type [[Some]] tuple with timestamp and [[ResultSet]] instance reference if
    *        entry exists or [[Null]] if there's no cached response for this task message
    */
  def ask(clientName: String, taskType: TaskMessageType): Option[(Long, R)] = {
    LOGGER.debug(s"Retrieving cached result for client, task type: $clientName, $taskType")
    get((clientName, taskType))
  }

  /**
    * Remove cached response
    *
    * @param clientName client name
    * @param taskType task message type [[TaskMessage]]
    */
  def remove(clientName: String, taskType: TaskMessageType): Unit = {
    LOGGER.debug(s"Removing cached result for client, task type: $clientName, $taskType")
    remove((clientName, taskType))
  }

  /**
    * Watchdog class that monitors cache lifetime
    *
    */
  private class DBCacheManager extends Runnable {
    override def run(): Unit = {
      while (true) {
        for (clientEntry <- toIterable) {
          val cacheEntry = clientEntry._2
          if ((System.currentTimeMillis() - cacheEntry._1) > cacheLifeTimeMS) {
            LOGGER.warn(s"Removing client entry from cache: ${clientEntry._1}")
            remove(clientEntry._1)
          }
        }

        Thread.sleep(cacheManagerHeartBeatMS)
      }
    }
  }
}
