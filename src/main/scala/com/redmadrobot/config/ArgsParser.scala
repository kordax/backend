package com.redmadrobot.config

import com.redmadrobot.chatlib.common.Constants
import org.log4s.getLogger

/**
  * Application arguments parser
  */
object ArgsParser {
  private val LOGGER = getLogger
  /** Invalid characters for argument value */
  private val HOST_INVALID_CHARS = Array("|/\\.!@#$%^&*()_+=-§±?><}{[]:;'\"~`".toCharArray)

  /** Current config */
  private var config = None: Option[Config]

  /** Current parser implementation */
  private val parser = new scopt.OptionParser[Config]("sbt run") {
    head(Constants.BACKEND_APP_NAME, Constants.WSPROXY_VERSION)

    opt[String]('h', "host").action((x, c) => c.copy(host = x)).text(s"remote rabbit mq host (DEFAULT: ${Defaults.CONFIG_HOST})")
    opt[Int]('p', "port").action((x, c) => c.copy(port = x)).text(s"remote rabbit mq host port (DEFAULT: ${Defaults.CONFIG_PORT})")
    opt[String]('u', "user").action((x, c) => c.copy(user = x)).text(s"remote rabbit mq user (DEFAULT: ${Defaults.CONFIG_USER})")
    opt[String]("password").action((x, c) => c.copy(pass = x)).text(s"remote rabbit mq port (DEFAULT: ${Defaults.CONFIG_PASS})")
    opt[String]('d', "dconn").action((x, c) => c.copy(dbConnStr = x)).text(s"database connection string (DEFAULT: ${Defaults.CONFIG_DB_CONN_STR})")
    opt[String]('k', "kafka").action((x, c) => c.copy(kafka = x)).text(s"single kafka broker (DEFAULT: ${Defaults.CONFIG_KAFKA_BROKER})")
    opt[Int]('n', "pool").action((x, c) => c.copy(poolSize = x)).text(s"workers pool size (DEFAULT: Number of cores * 2)")

    help("help").text(s"""EXAMPLE: sbt "run -h rabbit-mq.localhost -p 5672 -d ${Defaults.CONFIG_DB_CONN_STR}" """)
    version("version")
  }

  /**
    * Parses given arguments into internal config
    *
    * @param args arguments
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def parse(args: Array[String]): Unit = {
    LOGGER.debug("Parsing application arguments: " + args)
    parser.parse(args, Config()) match {
      case Some(parsedConf) =>
        config = Some(parsedConf)
        LOGGER.debug("Args passed: " + parsedConf)
      case None => throw new IllegalArgumentException("Passed arguments are illegal")
    }
  }

  /**
    * Validates arguments
    *
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  def validateArgs(): Unit = {
    LOGGER.debug("Validating application arguments: " + config)
    validateHost(config.get.host)
    validatePort(config.get.port)
    validateKafka(config.get.kafka)
  }

  /**
    * Get current config instance
    *
    * @return
    */
  def getConfig: Config = {
    LOGGER.debug("Getting config")
    config.get
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateHost(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }

  /**
    * Validates port
    *
    * @param port port
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validatePort(port: Int): Unit = {
    LOGGER.debug("Validating port: " + port)
    if (port < 1 || port > 65535) {
      LOGGER.error("port argument isn't valid")
      throw new IllegalArgumentException("port argument isn't valid")
    }
  }

  /**
    * Validates host
    *
    * @param host host
    * @throws java.lang.IllegalArgumentException if any error occurred
    */
  @throws(classOf[IllegalArgumentException])
  private def validateKafka(host: String): Unit = {
    LOGGER.debug("Validating host: " + host)
    if (host.toCharArray.contains(HOST_INVALID_CHARS)) {
      throw new IllegalArgumentException("host argument contains invalid characters")
    }

    for (c <- host.toCharArray) {
      if (c > 127) {
        LOGGER.error("host argument contains invalid characters")
        throw new IllegalArgumentException("host argument contains invalid characters")
      }
    }
  }
}
