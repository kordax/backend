package com.redmadrobot.config

/**
  * Configuration case class
  *
  * @param host host
  * @param port port
  */
case class Config(host: String = Defaults.CONFIG_HOST,
                  port: Int = Defaults.CONFIG_PORT,
                  user: String = Defaults.CONFIG_USER,
                  pass: String = Defaults.CONFIG_PASS,
                  dbConnStr: String = Defaults.CONFIG_DB_CONN_STR,
                  kafka: String = Defaults.CONFIG_KAFKA_BROKER,
                  poolSize: Int = Defaults.CONFIG_WORKERS_POOL_SIZE)