package com.redmadrobot.config

import com.redmadrobot.chatlib.adapters.db.PostgreSQLAdapter

/**
  * Config defaults
  */
object Defaults {
  /** Default configuration host */
  val CONFIG_HOST = ""
  /** Default configuration port */
  val CONFIG_PORT = 5672
  /** Default configuration host */
  val CONFIG_USER = "rmr"
  /** Default configuration port */
  val CONFIG_PASS = "rmr"
  /** Default db connection string */
  val CONFIG_DB_CONN_STR = s"jdbc:postgresql://localhost:5432/${PostgreSQLAdapter.DB_DEFAULT_DB_NAME}?user=${PostgreSQLAdapter.DB_DEFAULT_DB_NAME}"
  /** Default kafka broker string */
  val CONFIG_KAFKA_BROKER = ""
  /** Default zookeeper addr string */
  val CONFIG_WORKERS_POOL_SIZE = 0
}